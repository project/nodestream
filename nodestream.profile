<?php

/**
 * @file
 * This is the installation profile for NodeStream.
 */

/**
 * Implementation of hook_profile_details().
 */
function nodestream_profile_details() {
  return array(
    'name' => 'NodeStream',
    'description' => 'NodeStream is a Drupal distribution aimed at newspaper sites or sites with newspaper like content.',
  );
}

/**
 * Implementation of hook_profile_modules().
 *
 * Note that modules defined here is not hard dependencies. Additional modules
 * will be required as dependencies to other modules here.
 */
function nodestream_profile_modules() {
  return array(
    // Core.
    'dblog',
    'help',
    'menu',
    'path',
    'poll',
    'taxonomy',

    // Contrib.
    'ad',
    'admin',
    'chart',
    'conditional_fields',
    'content',
    'ctools',
    'devel',
    'diff',
    'disqus',
    'draggableviews',
    'draggableviews_cck',
    'dynamic_emfield',
    'dynamic_filefield',
    'dynamic_formatters',
    'email',
    'emfield',
    'emimage',
    'emvideo',
    'exportables',
    'features',
    'filefield',
    'geotaxonomy',
    'imageapi',
    'imageapi_gd',
    'imagecache',
    'imagecache_ui',
    'imagefield',
    'input_formats',
    'jquery_ui',
    'jquery_update',
    'modalframe',
    'nodereference',
    'noderelationships',
    'openlayers',
    'openlayers_cck',
    'openlayers_views',
    'openlayers_ui',
    'optionwidgets',
    'page_manager',
    'panels',
    'panels_everywhere',
    'panels_mini',
    'pathauto',
    'text',
    'token',
    'twitter',
    'scheduler',
    'semanticviews',
    'strongarm',
    'vertical_tabs',
    'views',
    'views_content',
    'views_slideshow',
    'views_slideshow_singleframe',
    'views_ui',
    'webform',
    'wysiwyg',

    // NodeStream.
    'ns_article',
    'ns_blog',
    'ns_contributor',
    'ns_core',
    'ns_fact',
    'ns_form',
    'ns_image',
    'ns_input_formats',
    'ns_layout',
    'ns_location',
    'ns_page',
    'ns_poll',
    'ns_video',
  );
}

/**
 * Implementation of hook_profile_task_list().
 */
function nodestream_profile_task_list() {
  global $conf;
  $conf['site_name'] = 'NodeStream installation guide';
  $conf['site_footer'] = 'NodeStream by <a href="http://nodeone.se">NodeOne</a>';
  return array();
}

/**
 * Helper function for fetching the order of demo content files.
 */
function nodestream_profile_content_files() {
  return array(
    'contributor',
    'video',
    'fact',
    'form',
    'article',
    'promo',
    'blog',
    'post',
    'ad',
  );
}

/**
 * Implementation of hook_profile_tasks().
 */
function nodestream_profile_tasks(&$task, $url) {
  if ($task == 'profile') {
    $batch = array();
    $batch['title'] = st('Configuring @drupal', array('@drupal' => drupal_install_profile_name()));
    $batch['error_message'] = st('There was an error configuring @drupal.', array('@drupal' => drupal_install_profile_name()));

    $batch['operations'][] = array('nodestream_profile_operation_prepare', array());
    $batch['operations'][] = array('nodestream_profile_operation_basic', array());
    $batch['operations'][] = array('nodestream_profile_operation_social', array());
    $batch['operations'][] = array('nodestream_profile_operation_taxonomy', array());
    foreach (nodestream_profile_content_files() as $file) {
      $batch['operations'][] = array('nodestream_profile_operation_content', array($file));
    }
    $batch['finished'] = 'nodestream_profile_operation_finished';

    variable_set('install_task', 'ns-batch');
    batch_set($batch);
    batch_process($url, $url);
    // Jut for cli installs.
    return;
  }

  // We are running a batch task for this profile so do nothing and return page.
  if ($task == 'ns-batch') {
    include_once 'includes/batch.inc';
    $output = _batch_page();
  }

  return $output;
}

/**
 * Preparing install operation.
 */
function nodestream_profile_operation_prepare(&$context) {
  $context['message'] = t('Preparing configuration.');
  drupal_flush_all_caches();
}

/**
 * Install operation for basic stuff.
 */
function nodestream_profile_operation_basic(&$context) {
  $context['message'] = t('Setting up basic features.');

  // Define Pathauto settings before saving vocabularies and terms.
  variable_set('pathauto_node_pattern', '');
  variable_set('pathauto_taxonomy_pattern', '');
  variable_set('pathauto_user_pattern', '');

  // Site front page.
  variable_set('site_frontpage', 'taxonomy/term/1');

  // Setup Geo Taxonomy to work with our recently created vocabulary.
  variable_set('geotaxonomy_3', TRUE);

  // We create the ad group vocabulary so we can ad some default groups.
  variable_set('ad_group_vid', 4);
  // Use the raw display format.
  variable_set('ad_display', 'raw');

  // Define some basic menu links.
  $menu_links = array(
    array(
      'link_title' => 'Front page',
      'link_path' => '<front>',
      'menu_name' => 'primary-links',
      'weight' => -10,
      'hidden' => FALSE,
    ),
    array(
      'link_title' => 'Business',
      'link_path' => 'taxonomy/term/2',
      'menu_name' => 'primary-links',
      'weight' => -9,
      'hidden' => FALSE,
    ),
    array(
      'link_title' => 'Technology',
      'link_path' => 'taxonomy/term/3',
      'menu_name' => 'primary-links',
      'weight' => -8,
      'hidden' => FALSE,
    ),
  );

  // Save menu links.
  foreach ($menu_links as $menu_link) {
    menu_link_save($menu_link);
  }

  // Remove all blocks.
  db_query("TRUNCATE {blocks}");

  // Only admin users can create new user accounts on the site.
  variable_set('user_register', 0);

  // Date configuration.
  variable_set('configurable_timezones', 0);
  variable_set('date_first_day', 1);
  variable_set('date_format_short', 'Y-m-d H:i');
  variable_set('date_format_short_custom', 'Y-m-d H:i');
  variable_set('date_format_medium', 'D, Y-m-d H:i');
  variable_set('date_format_medium_custom', 'D, Y-m-d H:i');
  variable_set('date_format_long', 'l, j F, Y - H:i');
  variable_set('date_format_long_custom', 'l, j F, Y - H:i');

  // Scheduler date format.
  variable_set('scheduler_date_format', 'Y-m-d H:i');

  // Theme related stuff.
  variable_set('theme_default', 'ns_default');
  variable_set('admin_theme', 'ns_seven');
  variable_set('node_admin_theme', 1);

  // Setup the Admin toolbar.
  variable_set(
    'admin_toolbar',
    array(
      'layout' => 'vertical',
      'position' => 'nw',
      'blocks' => array(
        'admin-create' => -1,
        'admin-menu' => 1,
        'admin-devel' => -1,
        'ns_core-ns_core_region_editor' => -1,
        'ns_core-ns_core_local_tasks' => -1,
      ),
    )
  );

  // Setup conditional fields.
  conditional_fields_insert_field('ns_image', 'field_ns_image_url', 'field_ns_image_type', array('embed' => 'embed'));
  conditional_fields_insert_field('ns_image', 'field_ns_image_file', 'field_ns_image_type', array('local' => 'local'));

  // Setup node telationships and widgets.
  module_load_include('inc', 'noderelationships', 'noderelationships');
  noderelationships_settings_save(
    'ns_article',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_article_attachment' => 'noderelationships_noderef:page_grid',
          'field_ns_article_byline' => 'noderelationships_noderef:page_grid',
          'field_ns_article_image' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_article_attachment' => 'field_ns_article_attachment',
          'field_ns_article_byline' => 'field_ns_article_byline',
          'field_ns_article_image' => 'field_ns_article_image',
        ),
        'edit_reference' => array(
          'field_ns_article_attachment' => 'field_ns_article_attachment',
          'field_ns_article_byline' => 'field_ns_article_byline',
          'field_ns_article_image' => 'field_ns_article_image',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'ns_blog',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_blog_blogger' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_blog_blogger' => 'field_ns_blog_blogger',
        ),
        'edit_reference' => array(
          'field_ns_blog_blogger' => 'field_ns_blog_blogger',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'ns_image',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_image_credit' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_image_credit' => 'field_ns_image_credit',
        ),
        'edit_reference' => array(
          'field_ns_image_credit' => 'field_ns_image_credit',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'ns_post',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_post_blog' => 'noderelationships_noderef:page_grid',
          'field_ns_post_blogger' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_post_blog' => 'field_ns_post_blog',
          'field_ns_post_blogger' => 'field_ns_post_blogger',
        ),
        'edit_reference' => array(
          'field_ns_post_blog' => 'field_ns_post_blog',
          'field_ns_post_blogger' => 'field_ns_post_blogger',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'ns_promo',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_promo_article' => 'noderelationships_noderef:page_grid',
          'field_ns_promo_image' => 'noderelationships_noderef:page_grid',
          'field_ns_promo_parent' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_promo_article' => 'field_ns_promo_article',
          'field_ns_promo_image' => 'field_ns_promo_image',
          'field_ns_promo_parent' => 'field_ns_promo_parent',
        ),
        'edit_reference' => array(
          'field_ns_promo_article' => 'field_ns_promo_article',
          'field_ns_promo_image' => 'field_ns_promo_image',
          'field_ns_promo_parent' => 'field_ns_promo_parent',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'ns_video',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_ns_video_credit' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_ns_video_credit' => 'field_ns_video_credit',
        ),
        'edit_reference' => array(
          'field_ns_video_credit' => 'field_ns_video_credit',
        ),
      ),
    )
  );

  // Delete the default webform content type.
  node_type_delete('webform');

  variable_set('emfield_emimage_allow_flickr', 1);
  variable_set('emfield_emimage_allow_photobucket', 1);
  variable_set('emfield_emimage_allow_picasa', 1);

  // Enable some page panels.
  variable_set('page_manager_term_view_disabled', 0);
  variable_set('page_manager_node_view_disabled', 0);
  variable_set('panels_everywhere_site_template_enabled', 1);
}

/**
 * Install operation for setting up social features.
 */
function nodestream_profile_operation_social(&$context) {
  $context['message'] = t('Setting up social features.');

  variable_set('twitter_default_format', '!title !tinyurl');
  variable_set(
    'twitter_types',
    array(
      'ns_article' => 'ns_article',
      'ns_blog' => 0,
      'ns_contributor' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_image' => 0,
      'ns_location' => 0,
      'ns_page' => 0,
      'ns_post' => 'ns_post',
      'ns_promo' => 0,
      'ns_video' => 0,
      'poll' => 0,
    )
  );

  variable_set('disqus_location', 'block');
  variable_set(
    'disqus_nodetypes',
    array(
      'ns_article' => 'ns_article',
      'ns_blog' => 0,
      'ns_contributor' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_image' => 0,
      'ns_location' => 0,
      'ns_page' => 0,
      'ns_post' => 'ns_post',
      'ns_promo' => 0,
      'ns_video' => 0,
      'poll' => 0,
    )
  );
}

/**
 * Install operation for setting up the default taxonomy.
 */
function nodestream_profile_operation_taxonomy(&$context) {
  $context['message'] = t('Setting up default vocabularies.');

  require_once dirname(__FILE__) . '/taxonomy/vocabulary.inc';
  require_once dirname(__FILE__) . '/taxonomy/term.inc';

  foreach ($vocabularies as $vocabulary) {
    taxonomy_save_vocabulary($vocabulary);
  }

  foreach ($terms as $term) {
    taxonomy_save_term($term);
  }
}

/**
 * Install operation for setting up demo content.
 */
function nodestream_profile_operation_content($file, &$context) {
  $context['message'] = t('Setting up demo content.');
  $user = user_load(array('uid' => 1));

  require_once dirname(__FILE__) . '/content/' . $file . '.inc';

  foreach ($nodes as $node) {
    $node = (object)$node;
    node_save($node);
  }
}

/**
 * Last install operation.
 */
function nodestream_profile_operation_finished($success, $results, $operations) {
  node_access_rebuild();
  variable_set('install_task', 'profile-finished');
}
