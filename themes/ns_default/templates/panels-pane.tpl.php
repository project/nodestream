<?php
?>
<?php if (!empty($content)): ?>
  <div class="<?php print $classes; ?> clear-block" <?php print $id; ?>>
    <?php if ($admin_links): ?>
      <div class="admin-links panel-hide">
        <?php print $admin_links; ?>
      </div>
    <?php endif; ?>

    <?php if ($title): ?>
      <h2 class="pane-title"><?php print $title; ?></h2>
    <?php endif; ?>

    <?php print $content; ?>

    <?php if ($links): ?>
      <div class="pane-links">
        <?php print $links; ?>
      </div>
    <?php endif; ?>

    <?php if ($more): ?>
      <div class="pane-more">
        <?php print $more; ?>
      </div>
    <?php endif; ?>
  </div>
<?php endif; ?>
