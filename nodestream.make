; $Id$

; This make file contains patches and third party libraries that basically are
; required for NodeStream to work with all features. The reason for not being
; included in the drupal-org.make file is because of compatibility problems
; with the drupal.org packaging system.

; Core

core = "6.x"

projects[] = "drupal"

; Includes

includes[drupal-org] = "drupal-org.make"

; Patches

; http://drupal.org/node/885412
projects[disqus][patch][] = "http://drupal.org/files/issues/disqus-views-relationships.patch"

; http://drupal.org/node/624018
projects[wysiwyg][patch][] = "http://drupal.org/files/issues/wysiwyg-624018-with-ui-3.patch"

; Libraries

libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery-ui-1.7.3.zip"
libraries[jquery_ui][destination] = "modules/contrib/jquery_ui"
libraries[jquery_ui][directory_name] = "jquery.ui"

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "http://downloads.sourceforge.net/project/tinymce/TinyMCE/3.3.8/tinymce_3_3_8.zip"
libraries[tinymce][destination] = "libraries"
libraries[tinymce][directory_name] = "tinymce"
