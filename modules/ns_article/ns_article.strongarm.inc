<?php

/**
 * Implementation of hook_strongarm().
 */
function ns_article_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_article';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '7',
    'author' => '8',
    'options' => '3',
    'menu' => '5',
    'taxonomy' => '2',
    'path' => '6',
    'scheduler_settings' => '4',
  );

  $export['content_extra_weights_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_ns_promo';
  $strongarm->value = array(
    'title' => '5',
    'revision_information' => '15',
    'author' => '16',
    'options' => '12',
    'menu' => '13',
    'taxonomy' => '11',
    'path' => '14',
  );

  $export['content_extra_weights_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_ns_article';
  $strongarm->value = 1;

  $export['enable_revisions_page_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_ns_promo';
  $strongarm->value = 1;

  $export['enable_revisions_page_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_article';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_promo';
  $strongarm->value = array(
    0 => 'status',
  );

  $export['node_options_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_article';
  $strongarm->value = 1;

  $export['scheduler_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_ns_promo';
  $strongarm->value = 0;

  $export['scheduler_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_article';
  $strongarm->value = 1;

  $export['scheduler_touch_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_touch_ns_promo';
  $strongarm->value = 0;

  $export['scheduler_touch_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_article';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_ns_promo';
  $strongarm->value = 0;

  $export['show_diff_inline_ns_promo'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_article';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_article'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_ns_promo';
  $strongarm->value = 1;

  $export['show_preview_changes_ns_promo'] = $strongarm;
  return $export;
}
