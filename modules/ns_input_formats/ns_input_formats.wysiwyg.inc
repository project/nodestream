<?php

/**
 * Implementation of hook_default_wysiwyg().
 */
function ns_input_formats_default_wysiwyg() {
  $export = array();
  $wysiwyg = new stdClass;
  $wysiwyg->disabled = FALSE; /* Edit this to true to make a default wysiwyg disabled initially */
  $wysiwyg->api_version = 2;
  $wysiwyg->editor = 'tinymce';
  $wysiwyg->settings = array(
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'en',
    'buttons' => array(
      'default' => array(
        'bold' => 1,
        'italic' => 1,
        'bullist' => 1,
        'numlist' => 1,
        'undo' => 1,
        'redo' => 1,
        'link' => 1,
        'unlink' => 1,
        'copy' => 1,
        'paste' => 1,
        'help' => 1,
      ),
      'font' => array(
        'formatselect' => 1,
      ),
      'inlinepopups' => array(
        'inlinepopups' => 1,
      ),
      'paste' => array(
        'pasteword' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 0,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 0,
    'remove_linebreaks' => 0,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 0,
    'block_formats' => 'p,h2,h3,h4,h5,h6',
    'css_setting' => 'self',
    'css_path' => '%bprofiles/nodestream/themes/ns_default/styles/basic.css',
    'css_classes' => '',
  );
  $wysiwyg->machine = 'text_editor';

  $export['text_editor'] = $wysiwyg;
  return $export;
}
