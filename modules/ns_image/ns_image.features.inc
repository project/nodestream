<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function ns_image_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function ns_image_imagecache_default_presets() {
  $items = array(
    'grid-10' => array(
      'presetname' => 'grid-10',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '190',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-12' => array(
      'presetname' => 'grid-12',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '230',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-24' => array(
      'presetname' => 'grid-24',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '470',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-35' => array(
      'presetname' => 'grid-35',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '690',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-4' => array(
      'presetname' => 'grid-4',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '70',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-49' => array(
      'presetname' => 'grid-49',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '970',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-5' => array(
      'presetname' => 'grid-5',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '90',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-6' => array(
      'presetname' => 'grid-6',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '110',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'grid-8' => array(
      'presetname' => 'grid-8',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '150',
            'height' => '',
            'upscale' => 1,
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function ns_image_node_info() {
  $items = array(
    'ns_image' => array(
      'name' => t('Image'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
