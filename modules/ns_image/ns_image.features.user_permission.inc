<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_image_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_image content
  $permissions['create ns_image content'] = array(
    'name' => 'create ns_image content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  // Exported permission: delete any ns_image content
  $permissions['delete any ns_image content'] = array(
    'name' => 'delete any ns_image content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_image content
  $permissions['delete own ns_image content'] = array(
    'name' => 'delete own ns_image content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_image content
  $permissions['edit any ns_image content'] = array(
    'name' => 'edit any ns_image content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own ns_image content
  $permissions['edit own ns_image content'] = array(
    'name' => 'edit own ns_image content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  return $permissions;
}
