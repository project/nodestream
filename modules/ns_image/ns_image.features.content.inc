<?php

/**
 * Implementation of hook_content_default_fields().
 */
function ns_image_content_default_fields() {
  $fields = array();

  // Exported field: field_ns_image_caption
  $fields['ns_image-field_ns_image_caption'] = array(
    'field_name' => 'field_ns_image_caption',
    'type_name' => 'ns_image',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_ns_image_caption][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Caption',
      'weight' => '-1',
      'description' => '',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_ns_image_credit
  $fields['ns_image-field_ns_image_credit'] = array(
    'field_name' => 'field_ns_image_credit',
    'type_name' => 'ns_image',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 1,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'ns_contributor' => 'ns_contributor',
      'ns_article' => 0,
      'ns_audio' => 0,
      'book' => 0,
      'ns_external' => 0,
      'ns_fact' => 0,
      'ns_form' => 0,
      'ns_manual' => 0,
      'ns_image' => 0,
      'ns_page' => 0,
      'ns_location' => 0,
      'ns_local_banner' => 0,
      'ns_link' => 0,
      'ns_poll' => 0,
      'ns_blurb' => 0,
      'ns_correction' => 0,
      'ns_table' => 0,
      'ns_video' => 0,
      'ns_waby' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_ns_image_credit][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Credit',
      'weight' => 0,
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_ns_image_file
  $fields['ns_image-field_ns_image_file'] = array(
    'field_name' => 'field_ns_image_file',
    'type_name' => 'ns_image',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'png gif jpg jpeg',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => '0',
      'min_resolution' => '0',
      'alt' => '',
      'custom_alt' => 0,
      'title' => '',
      'custom_title' => 0,
      'title_type' => 'textfield',
      'default_image' => NULL,
      'use_default_image' => 0,
      'label' => 'File',
      'weight' => '-3',
      'description' => '',
      'type' => 'imagefield_widget',
      'module' => 'imagefield',
    ),
  );

  // Exported field: field_ns_image_type
  $fields['ns_image-field_ns_image_type'] = array(
    'field_name' => 'field_ns_image_type',
    'type_name' => 'ns_image',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => 'local|Local file
embed|3rd party image',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => 'local',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Type',
      'weight' => '-4',
      'description' => '',
      'type' => 'optionwidgets_buttons',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_ns_image_url
  $fields['ns_image-field_ns_image_url'] = array(
    'field_name' => 'field_ns_image_url',
    'type_name' => 'ns_image',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'emimage',
    'required' => '0',
    'multiple' => '0',
    'module' => 'emimage',
    'active' => '1',
    'widget' => array(
      'full_width' => '500',
      'full_height' => '800',
      'full_link' => 'provider',
      'preview_width' => '240',
      'preview_height' => '360',
      'preview_link' => 'node',
      'thumbnail_width' => '100',
      'thumbnail_height' => '180',
      'thumbnail_link' => 'node',
      'providers' => array(
        'flickr' => 'flickr',
        'photobucket' => 'photobucket',
        'picasa' => 'picasa',
      ),
      'emthumb' => 0,
      'emthumb_label' => '',
      'emthumb_description' => '',
      'emthumb_max_resolution' => '0',
      'emimport_image_path' => '',
      'emthumb_custom_alt' => 0,
      'emthumb_custom_title' => 0,
      'emthumb_store_local_thumbnail' => 1,
      'emthumb_start_collapsed' => 0,
      'default_value' => array(
        '0' => array(
          'embed' => '',
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'URL',
      'weight' => '-2',
      'description' => '',
      'type' => 'emimage_textfields',
      'module' => 'emimage',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Credit');
  t('File');
  t('Type');
  t('URL');

  return $fields;
}
