<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_blog_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_blog content
  $permissions['create ns_blog content'] = array(
    'name' => 'create ns_blog content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: create ns_post content
  $permissions['create ns_post content'] = array(
    'name' => 'create ns_post content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  // Exported permission: delete any ns_blog content
  $permissions['delete any ns_blog content'] = array(
    'name' => 'delete any ns_blog content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete any ns_post content
  $permissions['delete any ns_post content'] = array(
    'name' => 'delete any ns_post content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_blog content
  $permissions['delete own ns_blog content'] = array(
    'name' => 'delete own ns_blog content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_post content
  $permissions['delete own ns_post content'] = array(
    'name' => 'delete own ns_post content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_blog content
  $permissions['edit any ns_blog content'] = array(
    'name' => 'edit any ns_blog content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_post content
  $permissions['edit any ns_post content'] = array(
    'name' => 'edit any ns_post content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own ns_blog content
  $permissions['edit own ns_blog content'] = array(
    'name' => 'edit own ns_blog content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own ns_post content
  $permissions['edit own ns_post content'] = array(
    'name' => 'edit own ns_post content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  return $permissions;
}
