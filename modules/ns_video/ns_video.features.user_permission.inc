<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function ns_video_user_default_permissions() {
  $permissions = array();

  // Exported permission: create ns_video content
  $permissions['create ns_video content'] = array(
    'name' => 'create ns_video content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  // Exported permission: delete any ns_video content
  $permissions['delete any ns_video content'] = array(
    'name' => 'delete any ns_video content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own ns_video content
  $permissions['delete own ns_video content'] = array(
    'name' => 'delete own ns_video content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any ns_video content
  $permissions['edit any ns_video content'] = array(
    'name' => 'edit any ns_video content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own ns_video content
  $permissions['edit own ns_video content'] = array(
    'name' => 'edit own ns_video content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'writer',
    ),
  );

  return $permissions;
}
