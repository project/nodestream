<?php

/**
 * Implementation of hook_views_handlers().
 */
function ns_core_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'ns_core') . '/includes/views/handlers',
    ),
    'handlers' => array(
      'ns_core_views_handler_field_node' => array(
        'parent' => 'views_handler_field_node',
        'file' => 'ns_core_views_handler_field_node.inc'
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function ns_core_views_data_alter(&$data) {
  // We switch the field handler for node titles. Our own handler allows for
  // some HTML to be used in node titles.
  $data['node']['title']['field']['handler'] = 'ns_core_views_handler_field_node';
}
