<?php

class ns_core_views_handler_field_node extends views_handler_field_node {
  function render($values) {
    return $this->render_link(ns_core_filter_xss($values->{$this->field_alias}), $values);
  }
}
