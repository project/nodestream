
(function ($) {
  $(document).ready(function(){
    $('#block-ns_core-ns_core_region_editor a').bind('click', function() {
      // Build modal frame options.
      var modalOptions = {
        url: this.href,
        width: $(window).width() - 30,
        height: $(window).height() - 30,
        autoFit: true,
        onSubmit: onSubmitCallback
      };
      Drupal.modalFrame.open(modalOptions);
      return false;
    });
  });

  function onSubmitCallback(args, statusMessages) {
    location.reload();
  }
})(jQuery);
