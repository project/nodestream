<?php

/**
 * @file
 * Plugin to provide an relationship handler for terms from node.
 */

$plugin = array(
  'title' => t('Multiple terms from node'),
  'keyword' => 'terms',
  'description' => t('Adds taxonomy terms from a node context and separates them with "+" or "," depending on settings.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'context' => 'ns_core_terms_from_node_context',
  'settings form' => 'ns_core_terms_from_node_settings_form',
  'defaults' => array(
    'vid' => '',
    'operator' => '+',
  ),
);

/**
 * Return a new context based on an existing context.
 */
function ns_core_terms_from_node_context($context, $conf) {
  // Initate the context.
  $relationship = new ctools_context(array('terms', 'term'));
  $relationship->plugin = 'terms_from_node';

  if (empty($context->data)) {
    return $relationship;
  }

  // Parse out all tids.
  $relationship->tids = array();
  if (isset($context->data->taxonomy)) {
    foreach ($context->data->taxonomy as $term) {
      if ($term->vid == $conf['vid']) {
        // Set the first term.
        if (!isset($relationship->term)) {
          $relationship->term = $term;
        }
        $relationship->tids[] = $term->tid;
      }
    }
  }

  // Build the rest of the relationship context.
  $relationship->data = $relationship->term;
  $relationship->title = $relationship->term->name;
  $relationship->operator = $conf['operator'];
  $relationship->argument = implode($conf['operator'], $relationship->tids);

  return $relationship;
}

/**
 * Settings form for the relationship.
 */
function ns_core_terms_from_node_settings_form($conf) {
  $options = array();
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }

  $form['vid'] = array(
    '#title' => t('Vocabulary'),
    '#description' => t('Choose from which vocabularies multiple terms shall be extracted.'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $conf['vid'],
  );
  $form['operator'] = array(
    '#title' => t('Operator'),
    '#description' => t('Choose how multiple terms shall be separated. The specified format can then be used as a Views argument.'),
    '#type' => 'select',
    '#options' => array(
      '+' => t('OR'),
      ',' => t('AND'),
    ),
    '#default_value' => $conf['operator'],
  );

  return $form;
}
